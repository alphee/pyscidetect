from setuptools import setup, find_packages

if __name__ == "__main__":
    # Read long description from README.md
    with open("README.md", "r") as f:
        long_description = f.read()

    # Load the basic requirements
    with open("requirements.txt", "r") as f:
        base_reqs = [
            req
            for req in f.readlines()
            if not req.startswith("#") and not req.startswith("-")  # prevents comments from being read
        ]  # prevents switches such as -e from being read

    setup(
        name="pyscidetect",
        version="1.0.2",
        author=["Stephan Tulkens (Slimmer AI)", "Sohi Sudhir (Slimmer AI)"],
        author_email="spmss@slimmer.ai",
        description="Check whether a document is automatically generated.",
        url="https://www.slimmer.ai/",
        packages=find_packages(),
        long_description=long_description,
        install_requires=base_reqs,
    )
