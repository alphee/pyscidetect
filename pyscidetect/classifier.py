import re
from collections import Counter, defaultdict

import numpy as np
from typing import Generator, List, Tuple


ALPHA = re.compile(r"[^a-zA-Z\d]")
NUM = re.compile(r"\d+")


class LM:
    def __init__(self, n: int = 3) -> None:
        """This is a simple language model."""
        self.n = n
        self.memory = defaultdict(Counter)
        self.probas = None
        self.oov_focus = None
        self.oov_proba = 0

    def ngrams(self, words: List[str]) -> Generator[Tuple[Tuple[str], str], None, None]:
        """
        Get ngrams from a list of words.

        :param words: A list of words
        :returns: A generator over contexts and focus words.
        """
        words = tuple((["<S>"] * self.n) + words)
        for x in range(len(words) - self.n):
            yield words[x : x + self.n], words[x + self.n]

    def to_grams(self, doc: str) -> Generator[Tuple[Tuple[str], str], None, None]:
        """
        Turn a string into a generator over word ngrams.

        The string is preprocessed by lowercasing and by subbing numbers
        and non-alpha characters.

        :param doc: A document, represented as a string.
        :return: A generator over word ngrams.
        """
        words = NUM.sub(ALPHA.sub(" ", doc.lower()), "0").split()
        yield from self.ngrams(words)

    def fit(self, documents: List[str]) -> "LM":
        """Fit the LM to a set of documents."""
        for doc in documents:
            for gram, focus in self.to_grams(doc):
                self.memory[gram][focus] += 1

        self.probas = {}
        self.oov_focus = {}
        all_counts = 0
        for k, d in self.memory.items():
            total = sum(d.values())
            all_counts += total
            self.oov_focus[k] = min(1 / total, 0.0001)
            self.probas[k] = {k2: v / total for k2, v in d.items()}
        self.oov_proba = 1 / all_counts
        return self

    def score(self, documents: List[str]) -> np.ndarray:
        """
        Scores a list of documents.

        :param documents: The documents to score.
        :returns: A single score for each document.
        """
        doc_probas = []
        for doc in documents:
            token_probas = []
            for gram, focus in self.to_grams(doc):
                # The probability of an unknown gram
                # is 1 / total
                try:
                    gram_probas = self.probas[gram]
                except KeyError:
                    token_probas.append(self.oov_proba)
                    continue
                # Given a gram, the probability of an unknown focus
                # is 1 / #gram
                try:
                    token_probas.append(gram_probas[focus])
                except KeyError:
                    token_probas.append(self.oov_focus[gram])
            # If the document was empty
            if token_probas:
                doc_probas.append(np.mean(token_probas))
            else:
                doc_probas.append(0)

        return np.asarray(doc_probas)
