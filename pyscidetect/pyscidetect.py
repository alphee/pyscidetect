from typing import List, Tuple

import numpy as np
import json

from pyscidetect.classifier import LM


class PySciDetect:
    def __init__(self, documents: List[str], labels: np.ndarray, threshold: float) -> None:
        """
        A Scidetect model.

        :param documents: A list of fake documents.
        :param labels: The labels of the fake documents.
            These should correspond to string labels of whatever system generated them.
        :param threshold: The threshold to use during classification.
        """
        self.label_mapping = {k: idx for idx, k in enumerate(set(labels))}
        self.label_mapping["normal"] = len(self.label_mapping)
        self.label_mapping_inv = {idx: k for k, idx in self.label_mapping.items()}

        if len(documents) != len(labels):
            raise ValueError(f"len(labels) != len(documents): {len(documents)} and {len(labels)}")

        self.threshold = threshold

        labels = np.asarray([self.label_mapping[x] for x in labels])
        documents = np.asarray(documents)

        self.classifiers = []
        for label in sorted(set(labels.tolist())):
            c = LM(n=3)
            c.fit(documents[labels == label])
            self.classifiers.append(c)

    def score(self, documents: List[str]) -> Tuple[List[str], np.ndarray]:
        """
        Classify new documents against our set of documents.

        :param documents: The documents to classify.
        :return: The predicted label for each instance and the score for each instance.
        """
        scores = []
        for classifier in self.classifiers:
            scores.append(classifier.score(documents))

        return np.stack(scores).T

    def predict(self, documents: List[str]) -> Tuple[List[str], np.ndarray]:
        """
        Classify new documents against the set of flagged documents.

        :param documents: The documents to classify.
        :return: The predicted label for each instance and the score for each instance.
        """
        scores = self.score(documents)
        labels = scores.argmax(1)
        labels[scores.max(1) < self.threshold] = self.label_mapping["normal"]

        return [self.label_mapping_inv[label] for label in labels]

    @classmethod
    def from_json(cls, path: str, **kwargs) -> "PySciDetect":
        """Load a PyScidetect instance from a set of examples."""
        data, labels = zip(*json.load(open(path)))
        return cls(documents=data, labels=labels, **kwargs)
