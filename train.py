from requests.exceptions import ReadTimeout
from typing import Optional

from pyscidetect.pyscidetect import PySciDetect
from tika import parser


def read_pdf(path: str) -> Optional[str]:
    """
    Reads a pdf from a path.

    :param path: the path to a PDF file.
    :returns: the text in a PDF file.
    """
    try:
        return parser.from_file(path, requestOptions={"timeout": 2})["content"]
    except ReadTimeout:
        return None


if __name__ == "__main__":

    p = PySciDetect.from_json("samples/train_data.json", threshold=0.25)
